#include <kernel/timer.h>

static struct timer_t
timer_read_mtime()
{
  struct timer_t ret = {.time_lo = *MTIME_LO,
                        .time_hi = *MTIME_HI};
  return ret;
}

uint32_t
get_timer_lo()
{
  return *MTIME_LO;
}

static void
timer_write_time_cmp(struct timer_t timer)
{
  *MTIME_CMP_LO = timer.time_lo;
  *MTIME_CMP_HI = timer.time_hi;
} 

void
timer_init()
{
  timer_incr();
}

void
timer_incr()
{
  struct timer_t cur_time = timer_read_mtime();

  uint32_t prev_time = cur_time.time_lo;
  
  
  // TODO make sure this does't overflow
  cur_time.time_lo += TIME_TO_CTX_SWITCH;


  if(cur_time.time_lo < prev_time) {
    cur_time.time_hi += 1;
  }

  timer_write_time_cmp(cur_time);
}

