#include <kernel/printk/printk.h>
#include <kernel/printk/uart.h>

#include <kernel/printk/uart.h>

#include <stdarg.h>

#define INT_STR_SIZE 12
#define HEX_STR_SIZE 9

static void
__write_char_(const char c)
{
  uart_write_char(c);
}

static void
__write_str_(const char* s)
{
  uart_write_string(s);
}

static void
__write_int_(uint32_t i)
{
  if(i == 0) {
    __write_char_('0');
    return;
  }

  char str[INT_STR_SIZE];
  str[INT_STR_SIZE - 1] = '\0';
  int pos = 1;
  while(i) {
    str[INT_STR_SIZE - pos - 1] = (char) (i % 10) + '0';
    i /= 10;
    pos++;
  }
  __write_str_(str + (INT_STR_SIZE - pos));
}

/* 
 * This function takes a 32 bit unsigned integer and prints
 * out the hex value with all capital letters because lowercase
 * letters are clearly less superior
 * */

static void
__write_hex_(uint32_t i)
{
  if (i == 0) {
    __write_char_('0');
    return;
  }

  char str[HEX_STR_SIZE];
  str[HEX_STR_SIZE - 1] = '\0';
  int pos = 1;

  while(i) {
    uint8_t cur_digit = (i % 16);

    if(cur_digit < 10)
      cur_digit += '0';
    else
      cur_digit += ('A' - 10);
  
    str[HEX_STR_SIZE - pos - 1] = (char) cur_digit;
    i /= 16;
    pos++;
  }

  __write_str_(str + (HEX_STR_SIZE - pos));
}

void
printk(const char* format, ...)
{
  va_list args;
  va_start(args, format);
  while (*format != '\0') {
    if (*format == '%') {
      format++;
      switch (*format) {
        case 'c':
          {
            const char c = (char)va_arg(args, uint32_t);
            __write_char_(c);
            break;
          }
        case 'd':
          {
            uint32_t i = va_arg(args, uint32_t);
            __write_int_(i);
            break;
          }
        case 's':
          {
            const char* s = va_arg(args, const char *);
            __write_str_(s);
            break;
          }

        case 'x':
          {
            uint32_t i = va_arg(args, uint32_t);
            __write_hex_(i);
            break;
          }
      }
      format++;
    }
    else {
      __write_char_(*format);
      format++;
    }
  }
  va_end(args);
}
