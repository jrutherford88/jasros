//hifive1 freq: 17,422,745
// qemu freq: 65,000,000
// BAUD rate: 115200
#include <kernel/printk/uart.h>
#include <global.h>

void
uart_init()
{
  uint64_t divisor = (CLOCK_FREQ / BAUD_RATE) - 1;

  *UART_DIV = divisor & 0xFFFF;
  *TXCTRL |= 1;
  *RXCTRL |= 1;
}

uint8_t
uart_read_char()
{
  if((*RXDATA >> 31) & 1)
    return '\0';

  return *RXDATA & 0xFF;
}

void
uart_write_char(uint8_t byte)
{
  while(*TXDATA >> 31);
  *TXDATA = byte & 0xFF;
}

void
uart_write_string(const char *out)
{
  while(*out)
    uart_write_char(*out++);
}

void
uart_write_stringln(const uint8_t *out)
{
  uart_write_string(out);
  uart_write_string("\r\n");
}
