#include <kernel/mm/kmem.h>

#include <kernel/printk/printk.h>

#include <stdbool.h>

#define HEADER_SIZE sizeof(size_t)
#define ALIGN 4

extern size_t HEAP_START;
extern size_t HEAP_END;

static struct kmem_region_t*
free_list_begin()
{
  return free_list_head;
}

static struct kmem_region_t*
free_list_next(const struct kmem_region_t* const region)
{
  return region->next_;
}

static void
debug_region(const struct kmem_region_t* const region)
{
  printk("------------------------------\n");
  printk("region addr = 0x%x\n", region);
  printk("region size = %d\n", region->region_size_);
  printk("region next -> 0x%x\n", region->next_);
  printk("region prev -> 0x%x\n", region->prev_);
  printk("------------------------------\n");
}

static void
debug_free_list()
{
  struct kmem_region_t* i = free_list_begin();
  while(1) {
    //debug_region(i);
    i = free_list_next(i);
    if (i == free_list_begin()) {
      break;
    }
  }
}

/* insert_region_after
 *
 * Insert a region of memory to the free list after the specified region
 * |after|.
 *
 */
static void
insert_region_after(struct kmem_region_t* const after,
                    struct kmem_region_t* const insert)
{
  //printk("%s:\n", __func__);
  //debug_region(after);
  //debug_region(insert);
  after->next_->prev_ = insert;
  if (after->next_ == after) {
    after->next_ = insert;
    insert->next_ = after;
  }
  else {
    insert->next_ = after->next_;
  }
  insert->prev_ = after;
  after->next_ = insert;
}

/* create_free_list_region
 *
 * Create a region to add to the free list. It is assumed that kmem_init has
 * already been called (and thus free_list_head is already a valid pointer).
 *
 * @addr   - address to add to the free list. This is assumed to be an address
 *           that has been allocated with kmalloc and thus will not point to the
 *           book keeping bytes at the beginning of memory regions. This newly
 *           created block is appended to the free list.
 *
 * @return - N/A
 */
static void
create_free_list_region(void* addr)
{
  struct kmem_region_t* new_region = (struct kmem_region_t*)(addr - HEADER_SIZE);
  //printk("%s:\n", __func__);
  //debug_region(new_region);

  insert_region_after(free_list_head->prev_, new_region);

  return;
}

static struct kmem_region_t*
split_region(struct kmem_region_t* original, size_t requested_size)
{
  struct kmem_region_t* new_region;
  new_region = (struct kmem_region_t*)(((void*)original) +
                              (original->region_size_ - requested_size));
  new_region->region_size_ = requested_size;
  original->region_size_ = original->region_size_ - requested_size;
  insert_region_after(original, new_region);
  return new_region;
}

static void
remove_region_from_free_list(struct kmem_region_t* rm)
{
  rm->next_->prev_ = rm->prev_;
  rm->prev_->next_ = rm->next_;
  rm->prev_ = NULL;
  rm->next_ = NULL;
}

static struct kmem_region_t*
fit_free_list_node_to_size(size_t bytes)
{
  int target_size = ((bytes + HEADER_SIZE) + (ALIGN - 1)) & -ALIGN;

  struct kmem_region_t* i = free_list_begin();
  struct kmem_region_t* candidate_region = NULL;
  while(1) {
    if (i->region_size_ == target_size) {
      return i;
    } else if (i->region_size_ > target_size &&
               (candidate_region == NULL ||
                i->region_size_ < candidate_region->region_size_)) {
      candidate_region = i;
    }
    i = free_list_next(i);
    if (i == free_list_begin()) {
      break;
    }
  }

  candidate_region = split_region(candidate_region, target_size);

  return candidate_region;
}

/* kmem_init
 *
 * Initialize kmem with the head of the free list pointing to the start of the
 * heap, defined in src/asm/mem.S. This function _must_ be called before any
 * subsequent calls to kmalloc/kfree/coalesce.
 *
 */
void
kmem_init()
{
  free_list_head = (struct kmem_region_t*)HEAP_START;
  free_list_head->region_size_ = HEAP_END - HEAP_START;
  free_list_head->next_ = free_list_head;
  free_list_head->prev_ = free_list_head;

  //debug_region(free_list_head);

  //printk("free_list_head at initialization: %d bytes\n", free_list_head->region_size_);
  //printk("free_list_head at initialization: prev->0x%x\n", free_list_head->prev_);
  //printk("free_list_head at initialization: next->0x%x\n", free_list_head->next_);
}

struct kmem_region_t* join_regions(struct kmem_region_t* join,
                                   struct kmem_region_t* del)
{
  if (join == NULL || del == NULL) {
    return NULL;
  }

  //printk("%s: joining 0x%x and 0x%x\n", __func__, join, del);

  join->region_size_ += del->region_size_;

  del->prev_->next_ = del->next_;
  del->next_->prev_ = del->prev_;

  return join;
}

/* kmalloc
 *
 * Allocate the requested size of memory from the free list and return it to
 * the caller. If the requested amount is not available, return NULL
 *
 * @bytes - The amount of requested memory, in bytes
 *
 * @return - pointer to the newly allocated memory, NULL if failure.
 */
void*
kmalloc(size_t bytes)
{
  struct kmem_region_t* alloc_region = fit_free_list_node_to_size(bytes);

  //printk("%s: region to alloc =>\n", __func__);
  //debug_region(alloc_region);

  if (alloc_region == NULL) return NULL;

  remove_region_from_free_list(alloc_region);

  //printk("%s:\n", __func__);
  //debug_region(alloc_region);

  return (void*)((void*)alloc_region) + HEADER_SIZE;
}

/* kfree
 *
 * Deallocate memory pointed to by |region|. This means it will be added to the
 * free list to be reused later or coalesced after a certain amount of time.
 *
 * @region - region of heap memory allocated using kmalloc
 *
 * @return - N/A
 */
void
kfree(void* region) {
  create_free_list_region(region);
  //printk("%s: printing new free list:\n", __func__);
  //debug_free_list();
  return;
}

void
coalesce()
{
  //printk("%s: starting coalesce\n", __func__);
  for (struct kmem_region_t* i = free_list_begin();
       i != free_list_begin()->prev_;
       i = free_list_next(i)) {
    //printk("i:\n");
    //debug_region(i);
    for (struct kmem_region_t* j = free_list_next(i);
         j != free_list_begin();
         j = free_list_next(j)) {
      //printk("j:\n");
      //debug_region(j);
      if (((void*)i + i->region_size_) == j) {
        i = join_regions(i, j);
        j = free_list_next(i);
      }
    }
  }

  //printk("%s: finished coalesce printing new free list:\n", __func__);
  //debug_free_list();
}
