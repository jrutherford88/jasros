#include <kernel/interrupt.h>
#include <kernel/timer.h>

#include <kernel/printk/printk.h>

enum interrupt_t {
  USER_SOFTWARE_INT   = 0,
  SUP_SOFTWARE_INT    = 1,
  RESERVED_2_INT      = 2,
  MACH_SOFTWARE_INT   = 3,
  USER_TIMER_INT      = 4,
  SUP_TIMER_INT       = 5,
  RESERVED_6_INT      = 6,
  MACH_TIMER_INT      = 7,
  USER_EXTERNAL_INT   = 8,
  SUP_EXTERNAL_INT    = 9,
  RESERVED_10_INT     = 10,
  MACH_EXTERNAL_INT   = 11,
  RESERVED_12_INT     = 12,
  RESERVED_13_INT     = 13,
  RESERVED_14_INT     = 14,
  RESERVED_15_INT     = 15,
  RESERVED_16_INT     = 16,
};

enum exception_t {
  INSTR_ADDR_MISALIGNED     = 0,
  INSTR_ACCESS_FAULT        = 1,
  ILLEGAL_INSTRUCTION       = 2,
  BREAKPOINT                = 3,
  LOAD_ADDR_MISALIGNED      = 4,
  LOAD_ACCESS_FAULT         = 5,
  STOREAMO_ADDR_MISALIGNED  = 6,
  STOREAMO_ACCESS_FAULT     = 7,
  USER_MODE_ECALL           = 8,
  SUP_MODE_ECALL            = 9,
  RESERVED_10_EXCEPT        = 10,
  MACH_MODE_ECALL           = 11,
  INSTR_PAGE_FAULT          = 12,
  LOAD_PAGE_FAULT           = 13,
  RESERVED_14_EXCEPT        = 14,
  STOREAMO_PAGE_FAULT       = 15,
};

uint32_t
handle_trap(uint32_t mcause, uint32_t mepc)
{
  uint8_t interrupt_flag = mcause >> 31;

  // Clear CLINT so we don't get stuck on this interrupt.
  *CORE_LOCAL_INTERRUPT_MAP = 0;
  
  if(interrupt_flag == 1) {
    enum interrupt_t mcause_code = mcause & 0x1F;
    switch(mcause_code) {
      case USER_SOFTWARE_INT:
        printk("User software interrupt!\n");
        break;
      case SUP_SOFTWARE_INT:
        printk("Supervisor software interrupt!\n");
        break;
      case MACH_SOFTWARE_INT:
        printk("Machine software interrupt!\n");
        break;
      case USER_TIMER_INT:
        printk("User timer interrupt!\n");
        break;
      case SUP_TIMER_INT:
        printk("Supervisor timer interrupt!\n");
        break;
      case MACH_TIMER_INT:
        timer_incr();
        return mepc;
      case USER_EXTERNAL_INT:
        printk("User external interrupt!\n");
        break;
      case SUP_EXTERNAL_INT:
        printk("Supervisor external interrupt!\n");
        break;
      case MACH_EXTERNAL_INT:
        printk("Machine external interrupt!\n");
        break;
      case RESERVED_2_INT:
      case RESERVED_6_INT:
      case RESERVED_10_INT:
      case RESERVED_12_INT:
      case RESERVED_13_INT:
      case RESERVED_14_INT:
      case RESERVED_15_INT:
      case RESERVED_16_INT:
        printk("Reserved for future standard use...\n");
        break;
      default:
        printk("ERROR: Interrupt: %d. Unimplemented MCAUSE_CODE: %d", interrupt_flag, mcause_code);
        break;
    }
  }
  else {
    enum exception_t mcause_code = mcause & 0x1F;
    switch(mcause_code) {
      case INSTR_ADDR_MISALIGNED:
        printk("Instruction address misaligned\n");
        break;
      case INSTR_ACCESS_FAULT:
        printk("Instruction access fault\n");
        break;
      case ILLEGAL_INSTRUCTION:
        printk("Illegal instruction\n");
        break;
      case BREAKPOINT:
        printk("Breakpoint\n");
        break;
      case LOAD_ADDR_MISALIGNED:
        printk("Load address misaligned\n");
        break;
      case LOAD_ACCESS_FAULT:
        printk("Load access fault\n");
        break;
      case STOREAMO_ADDR_MISALIGNED:
        printk("Store/AMO address misaligned\n");
        break;
      case STOREAMO_ACCESS_FAULT:
        printk("Store/AMO access fault\n");
        break;
      case USER_MODE_ECALL:
        printk("Environment call from U-mode\n");
        break;
      case SUP_MODE_ECALL:
        printk("Environment call from S-mode\n");
        break;
      case MACH_MODE_ECALL:
        printk("Environment call from M-mode\n");
        break;
      case INSTR_PAGE_FAULT:
        printk("Instruction page fault\n");
        break;
      case LOAD_PAGE_FAULT:
        printk("Load page fault\n");
        break;
      case STOREAMO_PAGE_FAULT:
        printk("Store/AMO page fault\n");
        break;
      case RESERVED_10_EXCEPT:
      case RESERVED_14_EXCEPT:
        printk("Reserved for future standard use\n");
        break;
      default:
        printk("ERROR: Exception: %d. Unimplemented MCAUSE_CODE: %d", interrupt_flag, mcause_code);
        break;
    }
  }

  // Compressed instructions have 0b11 in the two least significant bit
  // positions set. If the last instruction before the interrupt was
  // compressed, the instruction length is two bytes. Otherwise the length is
  // standard 4 bytes.
  if ((*(uint32_t*)mepc & 0x3) != 0) {
    return (mepc + 2);
  }
  else {
    return (mepc + 4);
  }
}
