#include <kernel/sched.h>
#include <kernel/timer.h>
#include <kernel/mm/kmem.h>
#include <kernel/printk/printk.h>


/*
 * Deletes the function (just sets the state to DEAD so it can be taken over by
 * another process)
 * */
void
del_process(struct process_t* process)
{
  process->state = DEAD;
}


/*
 * This function will set the process to sleeping, as well as mark the time the
 * process should be woken up at
 * */
void
sleep_process(struct process_t* process, uint32_t sleep_time)
{
  process->state = SLEEPING;
  process->sleep_time = get_timer_lo() / CLOCK_FREQ + sleep_time;
}


/*
 * Given the process, this function will update the process from sleeping to
 * running if it should be woken up
 * */
void
update_sleep_time(struct process_t* process)
{
  if(process != NULL && process->state == SLEEPING) {
    if((get_timer_lo() / CLOCK_FREQ) >= process->sleep_time) {
      process->state == RUNNING;
      process->sleep_time = 0;
    }
  }
}


/*
 * Initialize the scheduler
 * */

void
scheduler_init()
{
  uint32_t i = 0;
  process_list = (struct process_t*)kmalloc(sizeof(struct process_t) * MAX_PROCESSES);
  for(i = 1; i < MAX_PROCESSES; i++) {
    struct process_t *cur_process;
    cur_process = &process_list[i];
    del_process(cur_process);
  } 

  //TODO What about the init process...
  CURRENT_PROCESS = process_list + 0;
  printk("Current process starting at: %d\n", *CURRENT_PROCESS);
  printk("Current process starting at: %x\n", *CURRENT_PROCESS);
}


/*
 * What regs[RA] should be set to for each process
 * TODO
 * */

void
recover() 
{
  
}

/*
 * Schedules a new process
 * */
void
scheduler_new_process(void (*func)(), const char *name, int32_t priority) {
  
  struct process_t *new_process;
  uint32_t i = 0;
  
  for(i = 0; i < MAX_PROCESSES; i++) {
    if(process_list[i].state == DEAD) {
      new_process = &process_list[i];
      new_process->pid = i + 1;
      new_process->program = func;
      new_process->state = RUNNING;
      new_process->start_time = get_timer_lo() / CLOCK_FREQ;
      new_process->regs[SP] = (uint32_t)STACK_SPACE + PROC_STACK_ALLOC * i - 1;
      new_process->regs[RA] = (uint32_t)recover;
      break;
    }
  }
}

struct process_t*
schedule()
{
  uint32_t i;
  for(i = 0; i < MAX_PROCESSES; i++) {
    update_sleep_time((struct process_t*)process_list + i);

    if(process_list[i].state == RUNNING)
      return process_list + i;

  }

  return process_list + 0;
}
