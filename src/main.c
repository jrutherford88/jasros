#include <global.h>
#include <kernel/interrupt.h>
#include <kernel/mm/kmem.h>
#include <kernel/printk/printk.h>
#include <kernel/printk/uart.h>
#include <kernel/sched.h>
#include <kernel/timer.h>

extern void enable_interrupts();

int
main()
{
  enable_interrupts();
  uart_init();
  timer_init();
  kmem_init();
  scheduler_init();
  
  printk("Hello world!\n");
  printk("Char: %c\n", 'a');
  printk("Number: %d\n", 88);
  printk("String: %s\n", "test");
  printk("Hex: %x\n", 1024);

  int* c = (int*) kmalloc(sizeof(int));
  *c = 5;
  printk("%d\n", *c);
  kfree(c);

  uint64_t* d = (int*) kmalloc(sizeof(uint64_t));
  *d = 51;
  printk("%d\n", *d);
  kfree(d);

  coalesce();

  while(1);
}
