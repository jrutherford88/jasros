#!/usr/bin/env sh

builddir="build"
cachefile=".build.cache"
targethost="riscv64-unknown-linux-gnu"
riscv64gdb=$(command -v riscv64-gdb) || exit 1
riscvqemu=$(command -v qemu-system-riscv32) || exit 1

if command -v "./config.guess" 2>&1 ; then
  buildhost="$(./config.guess)"
else
  buildhost=""
fi

sources="$(find . -name \*.c | sort)"
sources="${sources} $(find . -name \*.S | sort)"
sources=$(echo "${sources}" | sed -z '{ s/\n/ /g }')
sources=${sources%?}

blue="\e[36m"
red="\e[31m"
reset="\e[0m"

#echo "sources=${sources}"

new_git_sources="$(git ls-files -o --exclude-standard | grep \\.c | sed -z '{ s/\n/ /g}')"
new_git_sources="${new_git_sources%?}"

if [ -f ${cachefile} ] ; then
  cache_sources="$(/bin/cat ${cachefile})"
fi

#echo "new_git_sources=$new_git_sources cache_sources=$cache_sources"
echo "${new_git_sources}" > ${cachefile}

jasros_src="$(awk '/jasros_SOURCES/{f=1}f' Makefile.am | tr '\\' ' ' | tr '\n' ' ' | cut -d '=' -f2 | sed '{ s/  / /g }')"
jasros_src="${jasros_src%?}"
#echo "jasros_src=${jasros_src}"

if { [ ! -z "${new_git_sources}" ] && [ "${cache_sources}" != "${new_git_sources}" ]; } || [ "${jasros_src}" != "${sources}" ] ; then
  sources=$(echo "${sources}" | sed -z '{ s/\n/\\\n/g }')
  sources=${sources%?}
  sed -n -i '{ /jasros_SOURCES/q;p }' Makefile.am
  echo "jasros_SOURCES=${sources}" >> Makefile.am
  autoreconf --install || exit 1

  if [ "${jasros_src}" != "${sources}" ] ; then
    make -C "${builddir}" clean
  fi
fi

if ! [ -d "${builddir}" ] ; then
  autoreconf --install || exit 1
  mkdir -p "${builddir}"
fi

if ! [ -f "${builddir}/Makefile" ] ; then
  cd "${builddir}" 
  ../configure --host="${targethost}" --build="${buildhost}"
  cd ..
fi

if [ $# -eq 1 ] ; then
  case "$1" in
    "all")
      make -C "${builddir}"
      if [ $? -eq 0 ] ; then
        echo -e "${blue}------------------- BUILD SUCCESSFUL -------------------${reset}"
      else
        echo -e "${red}------------------- BUILD FAILED -------------------${reset}"
        exit 1
      fi
      ;;
    "clean")
      make -C "${builddir}" clean
      ;;
    "distclean")
      make -C "${builddir}" clean
      make -C "${builddir}" distclean
      rm -rf "${builddir}"
      exit 0
      ;;
    "dist")
      make -C "${builddir}" dist
      exit 0
      ;;
    "gdb")
      make -C "${builddir}"
      if [ $? -eq 0 ] ; then
        echo -e "${blue}------------------- BUILD SUCCESSFUL -------------------${reset}"
      else
        echo -e "${red}------------------- BUILD FAILED -------------------${reset}"
        exit 1
      fi
      ${riscvqemu} -machine sifive_e -nographic -serial mon:stdio -kernel ${builddir}/jasros -S -s &
      ${riscv64gdb} ${builddir}/jasros -ex "target remote localhost:1234"
      exit 0
      ;;
    "qemu")
      make -C "${builddir}"
      if [ $? -eq 0 ] ; then
        echo -e "${blue}------------------- BUILD SUCCESSFUL -------------------${reset}"
      else
        echo -e "${red}------------------- BUILD FAILED -------------------${reset}"
        exit 1
      fi
      ${riscvqemu} -machine sifive_e -nographic -serial mon:stdio -kernel ${builddir}/jasros
      exit 0
      ;;
    *)
      echo "Unknown argument"
      exit 1
      ;;
  esac
elif [ $# -eq 0 ] ; then
  make -C "${builddir}"
  if [ $? -eq 0 ] ; then
    echo -e "${blue}------------------- BUILD SUCCESSFUL -------------------${reset}"
  else
    echo -e "${red}------------------- BUILD FAILED -------------------${reset}"
    exit 1
  fi
fi


exit 0
