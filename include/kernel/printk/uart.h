#ifndef __JASROS_UART_H__
#define __JASROS_UART_H__

#include <stdint.h>

static uint8_t *const UART_ADDR  = (uint8_t *)0x10013000;
static uint8_t *const TXDATA     = UART_ADDR + 0x00;
static uint8_t *const RXDATA     = UART_ADDR + 0x04;
static uint8_t *const TXCTRL     = UART_ADDR + 0x08;
static uint8_t *const RXCTRL     = UART_ADDR + 0x0c;
static uint8_t *const UART_IE    = UART_ADDR + 0x10;
static uint8_t *const UART_IP    = UART_ADDR + 0x14;
static uint8_t *const UART_DIV   = UART_ADDR + 0x18;

void uart_init();
uint8_t uart_read_char();
void uart_write_char(uint8_t byte);
void uart_write_string(const char *out);
void uart_write_stringln(const uint8_t *out);

#endif
