#ifndef __JASROS_PRINTK_H__
#define __JASROS_PRINTK_H__

#include <stdint.h>

void printk(const char* format, ...);

#endif //__JASROS_PRINTK_H__
