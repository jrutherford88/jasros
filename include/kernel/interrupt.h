#ifndef __JASROS_INTERRUPT_H__
#define __JASROS_INTERRUPT_H__

#include <global.h>
#include <stdint.h>

uint32_t handle_trap(uint32_t, uint32_t);

#endif
