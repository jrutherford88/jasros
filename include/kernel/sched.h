#ifndef __JASROS_SCHED_H__
#define __JASROS_SCHED_H__

#include <global.h>
#include <stdint.h>

enum registers {
  ZERO = 0,
  RA,
  SP,
  GP,
  TP,
  T0,
  T1,
  S0,
  S1,
  A0,
  A1,
  A2,
  A3,
  A4,
  A5,
  A6,
  A7,
  S2,
  S3,
  S4,
  S5,
  S6,
  S7,
  S8,
  S9,
  S10,
  S11,
  T3,
  T4,
  T5,
  T6,
};

enum process_state {
  DEAD = 0,
  EXITED,
  SLEEPING,
  RUNNING,
};

struct process_t {
  uint32_t             pid;
  // int32_t              priority;
  void                 (*program)();
  uint32_t             regs[32];
  uint32_t             run_time;
  uint32_t             start_time;
  // uint32_t             num_switches;
  uint32_t             sleep_time;
  enum process_state   state;
};

static struct process_t* process_list;
struct process_t* CURRENT_PROCESS;

void scheduler_init();


#endif
