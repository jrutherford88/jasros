#include <stdint.h>
#include <stddef.h>

struct kmem_region_t
{
  size_t region_size_;
  struct kmem_region_t* next_;
  struct kmem_region_t* prev_;
};

static struct kmem_region_t* free_list_head = NULL;

void kmem_init();
void* kmalloc(size_t bytes);
void kfree(void* mem);
void coalesce();
