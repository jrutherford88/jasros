#ifndef __JASROS_TIMER_H__
#define __JASROS_TIMER_H__

#include <stdint.h>
#include <global.h>

struct timer_t
{
  uint32_t time_lo;
  uint32_t time_hi;
};

static const uint64_t CTX_PER_SECOND = 1;
static const uint32_t TIME_TO_CTX_SWITCH = (uint32_t) (CLOCK_FREQ / CTX_PER_SECOND);
static uint32_t *const MTIME_CMP_LO = (uint32_t *) (CLINT_LOC + 0x4000);
static uint32_t *const MTIME_CMP_HI = (uint32_t *) (CLINT_LOC + 0x4004);
static uint32_t *const MTIME_LO     = (uint32_t *) (CLINT_LOC + 0xBFF8);
static uint32_t *const MTIME_HI     = (uint32_t *) (CLINT_LOC + 0xBFFC);

void timer_init();
void timer_incr();
uint32_t get_timer_lo();

#endif
