#ifndef __JASROS_GLOBAL_H__
#define __JASROS_GLOBAL_H__


#include <stdint.h>

#define MAX_PROCESSES 8
#define PROC_STACK_ALLOC 512


// Target BAUD rate for UART
static const uint32_t BAUD_RATE  = 115200;

// Master clock frequency in hertz
// static const uint32_t CLOCK_FREQ = 17422745;
static const uint32_t CLOCK_FREQ = 10000000;

// Address of the CLINT for software and timer interrupts
static const uint32_t CLINT_LOC = 0x02000000;
static uint32_t *const CORE_LOCAL_INTERRUPT_MAP = (uint32_t *) CLINT_LOC;


// static const uint32_t MAX_PROCESSES = 16;
// static const uint32_t PROC_STACK_ALLOC = 2 << 9;
static uint8_t STACK_SPACE[MAX_PROCESSES * PROC_STACK_ALLOC];

#endif

